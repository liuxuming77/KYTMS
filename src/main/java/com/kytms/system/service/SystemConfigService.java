package com.kytms.system.service;

import com.kytms.core.service.BaseService;

/**
 * 辽宁捷畅物流有限公司 -信息技术中心
 * @author 臧英明
 * @create 2017-11-20
 */
public interface SystemConfigService<Config> extends BaseService<Config> {
}
